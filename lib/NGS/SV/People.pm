package NGS::SV::People;
use Moose;
use namespace::sweep;
use NGS::SV::BasicSV;
use NGS::SV::Input;

has 'id' => (
    is   => 'ro',
    isa  => 'Str',
    required => 1,
    );

has '_variants' => (
    traits    => ['Array'],
    is   => 'ro',
    isa  => 'ArrayRef[NGS::SV::BasicSV]',
    default   => sub { [] },
    handles   => {
        addVariants    => 'push',
        nbVariants     => 'count',
        variants       => 'elements',
    },
    );

sub loadFromFile {
    my $self=shift;
    my $filename=shift;
    my $format=shift;
    my $minlength=shift;

    $self->addVariants(@{
	NGS::SV::Input->read({
	    'filename' => $filename,
	    'format' => $format,
	    'min-length' => $minlength,
			     });
		       });

}

sub BUILD {
    my $self = shift;
    my $args = shift;

    if (exists($args->{'filename'})) {
	$self->addVariants(@{
	    NGS::SV::Input->read($args);
			  });
    }
}

with (
    'NGS::SV::Roles::RecStringify',
    );

sub stringifySelf {
    my $self=shift;
    my $s=shift;
    
    my $res=$s->stringify("People: ".$self->id);
    if ($self->nbVariants && $s->level == 0) {
	$res->addLines( {
	    'first-prefix' => '  ',
		'prefix' => '  ',
			},
			$self->variants);
    }
    return $res;
}

1;
