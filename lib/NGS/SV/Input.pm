package NGS::SV::Input;
use Moose;
use namespace::sweep;

use strict;
use warnings;

use Module::Load 'load';
use Try::Tiny;

has 'filename' => (
    is    => 'rw',
    isa   => 'Str',
    required => 1,
    );

has 'format' => (
    is    => 'ro',
    isa   => 'Str',
    required => 1,
    );

has 'minlength' => (
    is    => 'ro',
    isa   => 'Int',
    required => 1,
    init_arg => 'min-length',
    );

sub read {
    my $self = shift;
    my $args = shift;

    if (! blessed($self)) {
	my $class=$self;
	$self = $class->new($args);
    }

    my $func = 'NGS::SV::Input::'.$self->format.'::read';
    my $sub = \&{$func};
    return $sub->($self->filename, $self->minlength);
}

sub BUILD {
    my $self=shift;

    my $module = 'NGS::SV::Input::'.$self->format;
    try {
	load $module;
    } catch {
	die "Cannot load $module. Did you specify an correct format?";
    }
}
1;


