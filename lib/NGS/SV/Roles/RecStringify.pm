package NGS::SV::Roles::RecStringify;
use MooseX::Role::WithOverloading;
use namespace::sweep;
use strict;
use warnings;
use NGS::SV::Utils::Stringify;

requires 'stringifySelf';

sub to_string {
    my $self=shift;
    my $s=new NGS::SV::Utils::Stringify();

    return $s->to_string($self);
}

use overload '""' => 'to_string', fallback => 1;

1;
