package NGS::SV::Roles::HasBasicSV;
use Moose::Role;
use namespace::sweep;

requires 'variant';

sub cmp {
    my $self=shift;
    my $other=shift;
    return $self->variant->cmp($other->variant);
}

sub cmpChr {
    my $self=shift;
    my $other=shift;
    return $self->variant->cmpChr($other->variant);
}

with (
    'NGS::SV::Roles::Comparator',
    );

1;
