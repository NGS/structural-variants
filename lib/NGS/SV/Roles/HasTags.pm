package NGS::SV::Roles::HasTags;
use Moose::Role;
use namespace::sweep;
use Carp;

has '_tags' => (
    traits    =>  ['Hash'],
    is        => 'rw',
    isa       => 'HashRef',
    required => 1,
    default  => sub { {} },
    handles  => {
        tag_names           => 'keys',
        tags                => 'elements',
        tag_pairs           => 'kv',
        has_no_tags         => 'is_empty',
        has_tags            => 'count',
        has_tag             => 'exists',
        tag0                => 'get',
        #all_named_values   => 'elements',
        #all_values         => 'values',
        #_register_value    => 'set',
        _add_valued_tag     => 'set',
        #_filter_types=> 'grep',
        #find_element   => 'first',
        #get_type    => 'get',
        #join_elements  => 'join',
        #count_types => 'count',
        #has_options    => 'count',
        #has_no_types=> 'is_empty',
        #sorted_options => 'sort',
    },
    lazy      => 1,
    );

sub _reset_tags {
    my $self = shift;
    $self->_tags( { @_ } );
}

sub _add_simple_tag {
    my $self = shift;
    my $name = shift;
    return $self->_add_valued_tag($name, undef);
}

sub _merge_tags {
    my $self = shift;
    my $taggedObj = shift;
    return $self->_add_valued_tag($taggedObj->tags);
}

sub addTag {
    my $self = shift;
    foreach my $t (@_) {
	$self->_add_simple_tag($t);
    }
}

sub addValuedTag {
    my $self = shift;
    my $name = shift;
    my $value = shift;
    return $self->_add_valued_tag($name, $value);
}

sub doTag {
    my $self = shift;
    my $nbargs=scalar(@_);
    if ($nbargs == 1) {
	$self->addTag(@_);
    } else {
	$self->addValuedTag(@_);
    }
}

sub tagPush {
    my $self = shift;
    my $name = shift;
    my $value = shift;
    if (!$self->has_tag($name)) {
	$self->addValuedTag($name, [$value]);
    } else {
	push @{$self->tag0($name)}, $value;
    }
}

sub tagInc {
    my $self = shift;
    my $name = shift;
    $self->addValuedTag($name, ($self->tag0($name) // 0) + 1);
}

sub tag {
    my $self = shift;
    my $name = shift;
    if (!$self->has_tag($name)) {
        # return undef; # See page 199 of PBP.
	return ;
    }
    return $self->tag0($name) // "";
}

1;
