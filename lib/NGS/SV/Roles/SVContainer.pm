package NGS::SV::Roles::SVContainer;
use Moose::Role;
use namespace::sweep;
use NGS::SV;

has 'variant' => (
    is   => 'ro',
    isa  => 'NGS::SV::BasicSV',
    );

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;
    
    if ( @_ == 1 && blessed($_[0]) && $_[0]->isa('NGS::SV::BasicSV')) {
	return $class->$orig( variant => $_[0] );
    } else {
	return $class->$orig(@_);
    }
};

# Moose bug, the line must be added in the consumming class instead...
# http://www.perlmonks.org/?node_id=915620
#with qw(NGS::SV::Roles::HasVariant);

with (
    'NGS::SV::Roles::RecStringify',
    );

# Same moose bug...
sub stringifySelfRole {
    my $self=shift;
    my $s=shift;

#    my $res=$s->stringify("Bag(".$self->nbVariants."):");
#    if ($self->nbVariants) {
#	$res->addLines( {
#	    'first-prefix' => '  ',
#		'prefix' => '  ',
#			},
#			$self->variants);
    #   }
    return $s->stringify($self->variant);
    my $res=$s->concat($self->variant, 
			'[', 
			$s->join(", ",
				 map {
				     $s->concat($_, "=>", $self->tag($_))
				 }
				 (sort $self->tag_names)),
			']');
    return $res;
}

1;
