package NGS::SV::Roles::Comparator;
use MooseX::Role::WithOverloading;
use namespace::sweep;
use strict;

requires 'cmp';

use overload 'cmp' => 'cmp';

sub eq {
    my $self=shift;
    my $other=shift;
    return $self->cmp($other) == 0;
}

use overload 'eq' => 'eq';

sub ne {
    my $self=shift;
    my $other=shift;
    return $self->cmp($other) != 0;
}

use overload 'ne' => 'ne';

sub le {
    my $self=shift;
    my $other=shift;
    return $self->cmp($other) <= 0;
}

use overload 'le' => 'le';

sub lt {
    my $self=shift;
    my $other=shift;
    return $self->cmp($other) < 0;
}

use overload 'lt' => 'lt';

sub ge {
    my $self=shift;
    my $other=shift;
    return $self->cmp($other) >= 0;
}

use overload 'ge' => 'ge';

sub gt {
    my $self=shift;
    my $other=shift;
    return $self->cmp($other) > 0;
}

use overload 'gt' => 'gt';

1;
