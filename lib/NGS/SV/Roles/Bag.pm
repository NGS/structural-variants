package NGS::SV::Roles::Bag;
use MooseX::Role::Parameterized;

use Scalar::Util qw( blessed );
use NGS::SV;

parameter 'variantType' => (
    #isa      => 'NGS::SV::Roles::HasBasicSV',
    isa      => 'Str',
    required => 1,
    );

role {
    my $p = shift;
    my $variantType = $p->variantType;
    
    requires 'newTaggedVariant';

    if (!$variantType->does('NGS::SV::Roles::HasBasicSV')) {
	die "$variantType does not have role NGS::SV::Roles::HasBasicSV";
    }
    
    has '_variants' => (
	traits    => ['Array'],
	is   => 'ro',
	#isa  => ArrayRef[Implements[qw(NGS::SV::Roles::HasBasicSV)]],
	isa  => "ArrayRef[${variantType}]",
	handles   => {
	    nbVariants     => 'count',
	    variants       => 'elements',
	},
	);

    has 'id' => (
	is   => 'rw',
	required => 0,
	);

    with (
	'NGS::SV::Roles::HasTags',
	);
    
    around BUILDARGS => sub {
	my $orig  = shift;
	my $class = shift;
	my @params = @_;
	my $args = {};
	my @variants = ();
	my $has_variants = 0;
	
	my $handle_tagged_variants = sub {
	    $has_variants = 1;
	    foreach my $v (@_) {
		push @variants, $v;
	    }
	};
	my $handle_variants = sub {
	    $has_variants = 1;
	    foreach my $v (@_) {
		$handle_tagged_variants->($class->newTaggedVariant($v));
	    }
	};
	my $handle_people = sub {
	    $has_variants = 1;
	    my $people = shift;
	    $handle_variants->($people->variants);
	};
	
	if ( @_ == 1 && ref $_[0] eq 'HASH') {
	    $args = $_[0];
	    if (exists($args->{'_variants'})) {
		$handle_tagged_variants->(@{$args->{'_variants'}});
	    }
	    if (exists($args->{'people'})) {
		$handle_people->($args->{'people'});
	    }
	    if (exists($args->{'variants'})) {
		$handle_variants->(@{$args->{'variants'}});
	    }
	} else {
	    while (scalar(@params) > 0) {
		my $ref0=ref($params[0]);
		if (not $ref0) {
		    my $k=shift @params;
		    my $val=shift @params;
		    if ($k eq 'people') {
			$handle_people->($val);
		    } elsif ($k eq 'variants') {
			$handle_variants->(@{$val});
		    } elsif ($k eq '_variants') {
			$handle_tagged_variants->(@{$val});
		    } else {
			$args->{$k}=$val;
		    }
		    next;
		} elsif (blessed($params[0])) {
		    if ($params[0]->isa('NGS::SV::People')) {
			$handle_people->($params[0]);
			shift @params;
			next;
		    } elsif ($params[0]->isa('NGS::SV')) {
			$handle_variants->($params[0]);
			shift @params;
			next;
		    } elsif ($params[0]->does('NGS::SV::Roles::HasBasicSV')) {
			$handle_tagged_variants->($params[0]);
			shift @params;
			next;
		    }
		    die "NGS::SV::Bags: what about object ".$params[0]."?";
		}
		die "NGS::SV::Bags: what about ".$params[0]."?";
	    }
	}
	if (! $has_variants) {
	    die "NGS::SV::Bags: no variants!";
	}
	
	return $class->$orig(%{$args}, '_variants', [sort { $a->variant->cmp($b->variant) } @variants]);
    };

    sub forallVariants {
	my $self = shift;
	my $code = \&{shift @_};
	my @args = @_;
	
	for my $v ($self->variants) {
	    $code->($v, @args);
	}
    }

    sub intersect {
	my $self = shift;
	my $b1=$self;
	my $b2=shift;
	my $code = \&{shift @_};

	my $v1=$b1->_variants;
	my $v2=$b2->_variants;

	$self->intersectPos1($b2, sub {
	    my (undef, undef, undef, undef, $p1, $p2)=@_;
	    my $cmp=$v1->[$p1]->variant->P2->cmpInterval($v2->[$p2]->variant->P2);
	    if ($cmp == 0) {
		$code->($b1, $b2, $v1->[$p1], $v2->[$p2], $p1, $p2);
	    }
			     });
    }

    sub intersectPos1 {
	my $self = shift;
	my $b1=$self;
	my $b2=shift;
	my $code = \&{shift @_};

	my $v1=$b1->_variants;
	my $v2=$b2->_variants;

	$self->intersectChr($b2, sub {
	    my (undef, undef, $p1, $p2, $n1, $n2)=@_;

	    while ($p1 < $n1 && $p2 < $n2) {
		my $cmp = $v1->[$p1]->variant->P1->cmpInterval($v2->[$p2]->variant->P1);
		if ($cmp < 0) {
		    $p1++;
		} elsif ($cmp > 0) {
		    $p2++;
		} else {
		    my $s2=$p2;
		    my $vpP1=$v1->[$p1]->variant->P1;
		    do {
			$code->($b1, $b2, $v1->[$p1], $v2->[$p2], $p1, $p2);
			$p2++;
			last if ($p2 >= $n2);
			$cmp=$vpP1->cmpInterval($v2->[$p2]->variant->P1);
		    } while ($cmp == 0);
		    $p2=$s2;
		    $p1++;
		}
	    }
			    });
    }

    sub intersectChr {
	my $self = shift;
	my $b1=$self;
	my $b2=shift;
	my $code = \&{shift @_};

	my $v1=$b1->_variants;
	my $v2=$b2->_variants;

	my $n1=$b1->nbVariants;
	my $n2=$b2->nbVariants;
	
	my $p1=0;
	my $p2=0;

	while ($p1 < $n1 && $p2 < $n2) {
	    my $cmp = $v1->[$p1]->cmpChr($v2->[$p2]);
	    if ($cmp < 0) {
		$p1++;
	    } elsif ($cmp > 0) {
		$p2++;
	    } else {
		my $s1=$p1++;
		my $vs1=$v1->[$s1];
		my $s2=$p2++;
		while ($p1 < $n1 && $vs1->cmpChr($v1->[$p1])==0) {
		    $p1++;
		}
		while ($p2 < $n2 && $vs1->cmpChr($v2->[$p2])==0) {
		    $p2++;
		}
		$code->($b1, $b2, $s1, $s2, $p1, $p2);
	    }
	}
    }

    with (
	'NGS::SV::Roles::RecStringify',
    );

    sub stringifySelfRole {
	my $self=shift;
	my $s=shift;
	
	my $res=$s->stringify("Bag(".$self->nbVariants."):");
	if ($self->nbVariants) {
	    $res->addLines( {
		'first-prefix' => '  ',
		    'prefix' => '  ',
			    },
			    $self->variants);
	}
	return $res;
    }

};

1;
