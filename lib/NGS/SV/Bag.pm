package NGS::SV::Bag;
use Moose;
use NGS::SV::TaggedSV;

with (
    'NGS::SV::Roles::Bag' => {
	'variantType' => 'NGS::SV::TaggedSV',
    },
    );

sub newTaggedVariant {
    my $class=shift;
    my $variant=shift;
    return NGS::SV::TaggedSV->new($variant);
}

sub stringifySelf {
    my $self=shift;
    my $s=shift;

    return $self->stringifySelfRole($s);
}


1;

