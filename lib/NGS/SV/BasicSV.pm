package NGS::SV::BasicSV;
use Moose;
use namespace::sweep;
use NGS::SV::Types;
use NGS::SV::Position;
use List::Util qw(all);

with (
    'NGS::SV::Roles::Comparator',
    );

for my $c ('P1', 'P2') {
    has $c => (
	is   => 'ro',
	isa  => 'NGS::SV::Position',
	required => 1,
	);
}


has 'length' => (
    is   => 'ro',
    isa  => 'Maybe[Int]',
    required => 0,
    default => undef,
    );

has 'supportingReads' => (
    is   => 'ro',
    isa  => 'Maybe[Int]',
    required => 0,
    default => undef,
    );


sub cmp {
    my $self=shift;
    my $other=shift;
    my ($sv1, $sv2) = ($self, $other);
    return ($sv1->P1->chr cmp $sv2->P1->chr)
	|| ($sv1->P2->chr cmp $sv2->P2->chr)
	|| ($sv1->P1 cmp $sv2->P1)
	|| ($sv1->P2 cmp $sv2->P2);
}

sub cmpChr {
    my $self=shift;
    my $other=shift;
    my ($sv1, $sv2) = ($self, $other);
    return ($sv1->P1->chr cmp $sv2->P1->chr)
	|| ($sv1->P2->chr cmp $sv2->P2->chr);
}

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;
    my $args;
    
    if ( @_ == 1 && ref $_[0] ) {
	$args = $_[0];
    } else {
	$args = { @_ };
    }
    foreach my $num (1, 2) {
	if (all { exists($args->{$_.$num}) } ('C', 'b', 'e')) {
	    $args->{'P'.$num} = NGS::SV::Position->new(
		'chr' =>  $args->{'C'.$num},
		'beg' =>  $args->{'b'.$num},
		'end' =>  $args->{'e'.$num});
	}
    }
    return $class->$orig($args);
};

sub to_string {
    my $self=shift;

    return "Variant(".$self->P1.", ".$self->P2.")";
}

use overload '""' => 'to_string', fallback => 1;

1;
