package NGS::SV::Types;

use Moose::Util::TypeConstraints;

subtype 'NGS::SV::Chromosome::Position',
    as 'Int',
    where { $_ >= 0 };

subtype 'NGS::SV::Chromosome',
    as 'Str';

1;
