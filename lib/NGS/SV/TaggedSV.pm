package NGS::SV::TaggedSV;
use Moose;
use namespace::sweep;
use NGS::SV;

with (
    'NGS::SV::Roles::SVContainer',
    );

with qw(NGS::SV::Roles::HasBasicSV);

with qw(NGS::SV::Roles::HasTags);

sub variantDecoration {
    my $self=shift;
    my $variantStr = shift;
    
    return $variantStr.'['.join(", ", map { $_."=>".$self->tag($_) } (sort $self->tag_names)).']';
}

sub stringifySelf {
    my $self=shift;
    my $s=shift;

    my $res=$s->concat($self->stringifySelfRole($s),
		       '[', 
		       $s->join(", ",
				map {
				    $s->concat($_, "=>", $self->tag($_))
				}
				(sort $self->tag_names)),
		       ']');
    return $res;
}

1;
