package NGS::SV::BagsCollection;
use Moose;
use namespace::sweep;
use NGS::SV::Bag;

has '_bags' => (
    traits    => ['Array'],
    is   => 'ro',
    isa  => 'ArrayRef[NGS::SV::Bag]',
    default   => sub { [] },
    handles   => {
        addBags        => 'push',
        nbBags         => 'count',
        bags           => 'elements',
    },
    );

sub bagsRef {
    my $self=shift;
    return $self->_bags;
}

sub addPeoples {
    my $self=shift;

    return $self->addBags(
	map { 
	    my $p=$_;
	    my $b=NGS::SV::Bag->new('id' => $p, $p);
	    $b->forallVariants( sub {
		my $v=shift;
		$v->doTag("fromPeople", $p);
				});
	    $b;
	} @_
	);
}

sub foreachBag {
    my $self=shift;
    my $code = \&{shift @_};

    foreach my $b ($self->bags) {
	$code->($b);
    }
}
    
sub filter {
    my $self=shift;
    my $code = \&{shift @_};

    my @filtbags=map {
	my $b=$_;
	my @variants;
	$b->forallVariants( sub {
	    my $v=shift;
	    if ($code->($v, $b)) {
		push @variants, $v;
	    }
			    });
	NGS::SV::Bag->new(
	    '_variants' => \@variants,
	    'id' => $b,
	    );
    } $self->bags;
    return NGS::SV::BagsCollection->new('_bags' => \@filtbags);
}
    
sub filterBags {
    my $self=shift;
    my $code = \&{shift @_};

    my @filtbags=map {
	my $b=$_;
	if ($code->($b)) {
	    return $b;
	}
	return;
    } $self->bags;
    return NGS::SV::BagsCollection->new('_bags' => \@filtbags);
}
    
sub intersect {
    my $self=shift;
    my $code = \&{shift @_};
    my $nb_bags=$self->nbBags;
    my $bags=$self->bagsRef;
    for(my $i=0; $i<$nb_bags; $i++) {
	for (my $j=$i+1; $j<$nb_bags; $j++) {
	    $bags->[$i]->intersect($bags->[$j], sub {
		$code->(@_);
				   });
	}
    }
}

sub foreachVariant {
    my $self=shift;
    my $code = \&{shift @_};
    map {
	my $b=$_;
	$b->forallVariants( sub {
	    my $v=shift;
	    $code->($b, $v);
			    });
    } $self->bags;

}

sub forallVariants {
    my $self=shift;
    my $code = \&{shift @_};
    map {
	my $b=$_;
	$b->forallVariants( sub {
	    my $v=shift;
	    $code->($b, $v);
			    });
    } $self->bags;

}

sub countDuplicates {
    my $self=shift;
    my $code = \&{shift @_ // sub {
	my ($b1, $b2, $v1, $v2)=@_;
	$v1->doTag("patients", $v1->tag("patients")."/".$v2->tag("fromPeople")->id);
	$v2->doTag("patients", $v2->tag("patients")."/".$v1->tag("fromPeople")->id);	
		  } };
    my $countvar=shift // "count";

    $self->forallVariants(sub {
	my (undef,$v)=@_;
	if (!$v->has_tag($countvar)) {
	    $v->doTag($countvar, 1);
	    $v->doTag("patients", $v->tag("fromPeople")->id);
	}
			  });
    $self->intersect( sub {
	my ($b1, $b2, $v1, $v2)=@_;
	$v1->tagInc($countvar);
	$v2->tagInc($countvar);
	$code->($b1, $b2, $v1, $v2);
		      });
    
}

sub BUILD {
    my $self = shift;
    my $args = shift;

    if (exists($args->{'peoples'})) {
	$self->addPeoples(@{$args->{'peoples'}});
    }
}

with (
    'NGS::SV::Roles::RecStringify',
    );

sub stringifySelf {
    my $self=shift;
    my $s=shift;
    
    my $res=$s->stringify("BagsCollection(".$self->nbBags."):");
    if ($self->nbBags) {
	$res->addLines( {
	    'first-prefix' => '* ',
		'prefix' => '  ',
			},
			$self->bags);
    }
    return $res;
}

1;
