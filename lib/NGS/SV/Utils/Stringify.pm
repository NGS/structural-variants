package NGS::SV::Utils::Stringify;
use Moose;
use namespace::sweep;
use Scalar::Util qw( blessed );
use Carp;

has '_prefixes' => (
    traits    => ['Array'],
    is   => 'ro',
    isa  => 'ArrayRef[Str]',
    default   => sub { [] },
    handles   => {
        pushPrefix     => 'push',
        levelp          => 'count',
        prefixes       => 'elements',
	prefix         => 'get',
    },
    );

has 'level' => (
    is  => 'rw',
    isa => 'Int',
    default => 0,
    required => 1,
    );

sub _recurse_enter {
    my $self=shift;
    my $args=shift;

    $self->level($self->level+1);
}

sub _recurse_exit {
    my $self=shift;
    my $args=shift;

    $self->level($self->level-1);
}

sub _set_default_args {
    my $self=shift;
    my $args=shift;
    return {
	'prefix' => '  ',
	    'sep' => '',
	    %{$args},
    };
}

sub _ensure_stringify {
    my $self=shift;
    my $obj = shift;
    my $args = shift;

    if (blessed($obj)) {
	if ($obj->isa('NGS::SV::Utils::Stringify::Lines')) {
	    return $obj;
	} elsif ($obj->does('NGS::SV::Roles::RecStringify')) {
	    my $lines=$obj->stringifySelf($self);
	    $self->_ensure_result_type($lines);
	    return $lines;
	}
    }
    my $lines=NGS::SV::Utils::Stringify::Lines->new(
	'_s' => $self,
	'_lines' => [
	    split("\n", "".$obj)
	]);
    $self->_ensure_result_type($lines);
    return $lines;
}

sub stringify {
    my $self=shift;
    my $obj=shift;
    my $args = $self->_set_default_args({@_});

    $self->_recurse_enter($args);

    my $lines=$self->_ensure_stringify($obj);
    $lines->_addPrefix($args->{'prefix'});
    $self->_recurse_exit($args);
    $self->_ensure_result_type($lines);
    return $lines;
}

sub concat {
    my $self=shift;

    return $self->join('', @_);
    my $args=shift;
    my @glines = @_;
    
    if (ref($args) ne "HASH") {
	unshift @glines, $args;
	$args = {};
    }
    $args=$self->_set_default_args($args);
    
    $self->_recurse_enter($args);
    @glines = map { $self->_ensure_stringify($_); } @glines;
    my $res;
    
    if (scalar(@glines) == 0) {
	return $self->_ensure_stringify("");
    } else {
	$res=shift @glines;
	for my $g (@glines) {
	    if ($res->count == 0) {
		$res=$g;
	    } elsif ($g->count > 0) {
		$res->_lines->[$res->count - 1] .= $g->shift;
		$res->addLines($args, $g);
	    }
	}
    }
    
    $self->_recurse_exit($args);
    return $res;    
}

sub join {
    my $self=shift;
    my $str=shift;
    my $args=shift;
    my @glines = @_;
    
    if (ref($args) ne "HASH") {
	unshift @glines, $args;
	$args = {};
    }
    $args=$self->_set_default_args($args);
    
    $self->_recurse_enter($args);
    $str = $self->_ensure_stringify($str);
    @glines = map { ($self->_ensure_stringify($_), $str) ; } @glines;
    pop @glines;
    my $res;
    
    if (scalar(@glines) == 0) {
	return $self->_ensure_stringify("");
    } else {
	$res=shift @glines;
	for my $g (@glines) {
	    if ($res->count == 0) {
		$res=$g;
	    } elsif ($g->count > 0) {
		my $f=$g->shift;
		$res->_lines->[$res->count - 1] .= $f;
		$res->addLines($args, $g);
		$g->unshift($f);
	    }
	}
    }
    
    $self->_recurse_exit($args);
    return $res;    
}

sub _ensure_result_type {
    my $self=shift;
    my $lines=shift;

    if ((!blessed($lines)) or !$lines->isa('NGS::SV::Utils::Stringify::Lines')) {
	croak("Bad result type of stringifySelf");
    }
}

sub to_string {
    my $self=shift;
    my $obj=shift;

    my $lines=$obj->stringifySelf($self);
    $self->_ensure_result_type($lines);
    return CORE::join("\n", $lines->lines);
}

1;

package NGS::SV::Utils::Stringify::Lines;
use Moose;
use namespace::sweep;
use Carp;

has '_s' => (
    is   => 'ro',
    isa  => 'NGS::SV::Utils::Stringify',
    required => 1,
    );

has '_lines' => (
    traits    => ['Array'],
    is   => 'ro',
    isa  => 'ArrayRef[Str]',
    default   => sub { [] },
    handles   => {
        'push'     => 'push',
	    'shift'    => 'shift',
	    'unshift'  => 'unshift',
	    'count'    => 'count',
	    'lines'    => 'elements',
	    'get'      => 'get',
	    'map'      => 'map',	    
    },
    );

sub _addPrefix {
    my $self=shift;
    my $prefix=shift;
    my $first=shift//1;

    for(my $l=$first; $l<$self->count; $l++) {
	$self->_lines->[$l]=$prefix.$self->_lines->[$l];
    }
    #carp "coucou";
    return;
#    my $first=1;
    foreach ($self->lines) {
	if ($first) {
	    $first=0;
	} else {
	    $_ = $prefix.$_;
	}
    }
}

sub addLines {
    my $self=shift;
    my $args=shift;

    my @glines = @_;
    
    if (ref($args) ne "HASH") {
	unshift @glines, $args;
	$args = {};
    }
    $args=$self->_s->_set_default_args($args);
    
    map {
	my $l=$_;
	$l->_addPrefix($args->{'prefix'}, 0);
	$self->push($l->lines);
    } (map { $self->_s->stringify($_) } @glines);

    return $self;
}

1;
