package NGS::SV::Input::General;

use strict;
use warnings;

# Input: a dir name, output an array of file names (one for each patient
# Case 1: all patients are in the same directory
sub PatientList1
{
    my $dir_name =  shift;
    my $identifier = shift;
    my @files;
    
    opendir DIR, $dir_name or die "cannot open dir $dir_name: $!";
    @files= map { $dir_name.'/'.$_ } readdir DIR;
    closedir DIR;
    
    my @kept_files = grep { $_ =~ /$identifier/ } @files;

#    foreach my $test (@kept_files) {
#	print STDERR $test, "  ";
#    }
#    print STDERR "\n";
    return \@kept_files;
    
}

# For Longranger: 
# Input: a dir name, output two array of file names, one with large SV calls and
# the other with large SV candidates
sub PatientListLR
{ 
    my $dir_name =  shift;
    my $identifier_calls = shift;
    my $identifier_candidates = shift;
    my @files;
    
    opendir DIR, $dir_name or die "cannot open dir $dir_name: $!";
    @files= map { $dir_name.'/'.$_ } readdir DIR;
    closedir DIR;
    
    my @kept_calls = grep { $_ =~ /$identifier_calls/ } @files;
    my @kept_candidates = grep { $_ =~ /$identifier_candidates/ } @files;

    print STDERR "KEPT CALLS\n";
    foreach my $test (@kept_calls) {
	print STDERR $test, "  ";
    }
    print STDERR "\n";
    print STDERR "KEPT CANDIDATES\n";
    foreach my $test (@kept_candidates) {
	print STDERR $test, "  ";
    }
    print STDERR "\n";
    
    return \@kept_calls, \@kept_candidates;
    
}


1;
