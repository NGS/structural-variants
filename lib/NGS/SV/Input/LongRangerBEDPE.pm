package NGS::SV::Input::LongRangerBEDPE;
use NGS::SV::BasicSV;

use strict;
use warnings;
use Carp;

sub read
{
    my $filename = shift;
    open(my $LR_BEDPE, '<', $filename)
	or croak "Can't open '$filename': $!";
    
    my ($ligne, @tab, @tab_var);
    while ($ligne =<$LR_BEDPE>) {
	chomp($ligne);
	next if ($ligne =~ /^#/);
	if ($ligne =~ /^chr/) {
	    @tab=split(/\t/, $ligne);
	    my $sv=NGS::SV::BasicSV->new(
		'C1' => $tab[0],
		'b1' => $tab[1],
		'e1' => $tab[2],
		'C2' => $tab[3],
		'b2' => $tab[4],
		'e2' => $tab[5]
		);
	    push(@tab_var, $sv);
	} else {
	    print STDERR "Strange line: ", $ligne, " \n";
	}
    }
    close($LR_BEDPE)
	or croak "Can't close '$filename': $!";
    return \@tab_var;
}

1;
