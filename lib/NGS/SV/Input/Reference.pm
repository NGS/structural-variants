package NGS::SV::Input::Reference;
use NGS::SV::BasicSV;

use strict;
use warnings;
use Carp;

sub read
{
    my $filename = shift;
    my $min_length = shift;
    my $count_variant_sup = 0;
  #  print STDERR (join (//, "~/recherche/Projets/NGS/10xChromium/SV_valide_Kevin/", $file);
    open(my $REFERENCE, '<',  $filename)
	or croak "Can't open '$filename': $!";
    
    my ($ligne, @tab, @tab_var);
    while ($ligne =<$REFERENCE>) {
	chomp($ligne);
	next if ($ligne =~ /^#/);
	if ($ligne =~ /^chr/) {
	    @tab=split(/\s+/, $ligne);
	    if (($tab[0] eq $tab[2]) && abs($tab[3]-$tab[1])< $min_length) {
		$count_variant_sup++;
		next;
	    } else {
		my $sv=NGS::SV::BasicSV->new(
		    'C1' => $tab[0],
		    'b1' => $tab[1],
		    'e1' => $tab[1],
		    'C2' => $tab[2],
		    'b2' => $tab[3],
		    'e2' => $tab[3]
		    );
		push(@tab_var, $sv);
	    }
	} else {
	    print STDERR "Strange line in file $filename: ", $ligne, " \n";
	}
    }
    close($REFERENCE)
	or croak "Can't close '$filename': $!";
     print STDERR "Nb REF variants filtered due to length < $min_length: $count_variant_sup \n";
 return \@tab_var;
}

1;
