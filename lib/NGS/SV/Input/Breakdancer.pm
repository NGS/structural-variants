package NGS::SV::Input::Breakdancer;
use NGS::SV::BasicSV;

use strict;
use warnings;
use Carp;

sub read
{
    my $filename = shift;
    my $min_length = shift;
    open(my $BREAKDANCER, '<', $filename)
	or croak "Can't open '$filename': $!";
    
    my ($ligne, @tab, @tab_var);
    my $count_variant_sup=0;
    while ($ligne =<$BREAKDANCER>) {
	chomp($ligne);
	next if ($ligne =~ /^#/);
	if ($ligne =~ /^chr/) {
	    @tab=split(/\t/, $ligne);
	    if (($tab[0] eq $tab[3]) && abs($tab[4]-$tab[1])< $min_length) {
		$count_variant_sup++;
		next;
	    } else {
		my $sv=NGS::SV::BasicSV->new(
		    'C1' => $tab[0],
		    'b1' => $tab[1],
		    'e1' => $tab[1],
		    'C2' => $tab[3],
		    'b2' => $tab[4],
		    'e2' => $tab[4]
		    );
		push(@tab_var, $sv);
	    }
	} elsif ($ligne =~ /^Chromosome_1\tPosition_1\t/) {
	    next;
	} else {
	    print STDERR "Strange line: ", $ligne, " \n";
	}
    }
    close($BREAKDANCER)
	or croak "Can't close '$filename': $!";
    print STDERR "Nb Breakdancer variants filtered due to length < $min_length: $count_variant_sup \n";
    return \@tab_var;
}

1;
