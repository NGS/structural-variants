package NGS::SV::Input::LumpyBEDPE;
use NGS::SV::BasicSV;

use strict;
use warnings;
use Carp;

sub read
{
    my $filename = shift;
    my $min_length = shift;
    open(my $LUMPYBEDPE, '<', $filename)
	or croak "Can't open '$filename': $!";
    
    my ($ligne, @tab, @tab_var);
    my $count_var_suppr = 0;
    while ($ligne =<$LUMPYBEDPE>) {
	chomp($ligne);
	next if ($ligne =~ /^#/);
	if ($ligne =~ /^chr/) {
	    @tab=split(/\t/, $ligne);
	    if ($tab[10] eq "BND") {
		 my $sv=NGS::SV::BasicSV->new(
		     'C1' => $tab[0],
		     'b1' => $tab[1],
		     'e1' => $tab[2],
		     'C2' => $tab[3],
		     'b2' => $tab[4],
		     'e2' => $tab[5]
		     );
		 push(@tab_var, $sv);
	    }
	    elsif (($tab[10] eq "DEL") || ($tab[10] eq "INV") ||  ($tab[10] eq "DUP")) {
		my @tab_tmp = split(/;/, $tab[12]);
		$tab_tmp[1] =~ /SVLEN=(-?[0-9]+)/;
		if (abs($1) < $min_length) {
		    $count_var_suppr++;
		    next;
		} else {
		    my $sv=NGS::SV::BasicSV->new(
			'C1' => $tab[0],
			'b1' => $tab[1],
			'e1' => $tab[2],
			'C2' => $tab[3],
			'b2' => $tab[4],
			'e2' => $tab[5]
			);
		    push(@tab_var, $sv);
		}
	    } else {
		print STDERR "Strange line A: ", $ligne, " \n";
	    } 
	} elsif ($ligne =~ /^hs37d5/) {
	    next;
	} else {
	    print STDERR "Strange line: ", $ligne, " \n";
	}
    }
    close($LUMPYBEDPE)
	or croak "Can't close '$filename': $!";
     print STDERR "Nb Lumpy-bedpe variants filtered due to length < $min_length: $count_var_suppr \n";
    return \@tab_var;
}

1;
