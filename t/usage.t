#!/usr/bin/perl

use strict;
use warnings;

use Test::More tests => 21;

package Disp;
use base 'Test::Builder::Module';

my $CLASS = __PACKAGE__;

sub is {
    my $got=shift;
    my @args=(@_);
    my $tb = $CLASS->builder;
    Test::More::diag($got);
    $tb->is_eq($got, @args);
}

package main;

use_ok('NGS::SV::People');
use_ok('NGS::SV::Input::LongRangerBEDPE');
use_ok('NGS::SV::Input::Breakdancer');
use_ok('NGS::SV::Input::LumpyBEDPE');

my $p1 = NGS::SV::People->new('id' => 'John');
my $p2 = NGS::SV::People->new(
    'id' => 'Jack',
    'filename' => 'fic_longranger2',
    'format' => 'LongRangerBEDPE',
    );
my $p3 = NGS::SV::People->new('id' => 'B00IOF8');
my $p4 = NGS::SV::People->new('id' => 'testlumpy');

my $tab_LR_var=NGS::SV::Input::LongRangerBEDPE::read("fic_longranger");
#my $tab_LR_var2=NGS::SV::Input::LongRangerBEDPE::read("fic_longranger2");
my $tab_BD_var=NGS::SV::Input::Breakdancer::read("fic_breakdancer");
my $tab_Lumpy_var=NGS::SV::Input::LumpyBEDPE::read("fic_lumpy_bedpe");

$p1->addVariants(@{$tab_LR_var});
#$p2->addVariants(@{$tab_LR_var2});
$p3->addVariants(@{$tab_BD_var});
$p4->addVariants(@{$tab_Lumpy_var});

Disp::is($p1."", "People: John
  Variant(chr3[162512135, 162512152], chr3[162626319, 162626335])
  Variant(chr6[157548448, 157548897], chr6[157613161, 157613245])
  Variant(chr1[152555542, 152555559], chr1[152587723, 152587740])
  Variant(chr9[66862793, 66862814], chr9[69000092, 69000113])
  Variant(chr11[54951647, 54951658], chr11[54982904, 54982920])",
   "People 1");
Disp::is($p2."", "People: Jack
  Variant(chr3[162512146, 162512178], chr3[162626345, 162626357])
  Variant(chr6[157548448, 157548897], chr8[157613161, 157613245])
  Variant(chr2[152555542, 152555559], chr2[152587723, 152587740])
  Variant(chr9[66862868, 66862926], chr9[69000133, 69000110])
  Variant(chr9[66863000, 66863050], chr9[69000150, 69000170])
  Variant(chr9[66863060, 66863070], chr9[69002150, 69002170])
  Variant(chr11[54951612, 54951698], chr11[54982905, 54982917])
  Variant(chr11[54950612, 54950698], chr11[54984905, 54984917])",
   "People 2");
Disp::is($p3."", "People: B00IOF8
  Variant(chr1[10001, 10001], chr1[10474, 10474])
  Variant(chr1[136710, 136710], chr1[136895, 136895])
  Variant(chr3[67135863, 67135863], chr3[67135904, 67135904])
  Variant(chr3[67493807, 67493807], chr3[67496770, 67496770])",
   "People 3");
Disp::is($p4."", "People: testlumpy
  Variant(chr1[869439, 869895], chr1[869862, 870285])
  Variant(chr1[1588111, 1588554], chr1[1653791, 1654160])
  Variant(chr1[36733269, 36733612], chr1[36733977, 36734516])
  Variant(chr1[36733239, 36733259], chr1[36734631, 36734651])
  Variant(chr1[37816462, 37816482], chr1[37816711, 37816731])",
   "People 4");

for my $t (
    [ $p1, 0, $p2, 0, 0],
    [ $p1, 1, $p2, 1, -1],
    [ $p1, 2, $p2, 2, -1],
    [ $p1, 3, $p2, 3, 0],
    [ $p1, 4, $p2, 6, 0],
    [ $p1, 4, $p2, 7, 1],
    ) {
    my ($p1, $nv1, $p2, $nv2, $expected) = @{$t};
    my $v1=($p1->variants)[$nv1];
    my $v2=($p2->variants)[$nv2];
    my $res=$v1 cmp $v2;
    is($res, $expected, $p1->id."(".$v1.") cmp ".$p2->id."(".$v2.")");
}

use_ok('NGS::SV::Bag');

my $b1 = NGS::SV::Bag->new($p1);
$b1->forallVariants( sub {
    my $v=shift;
    my $p=shift;
    $v->doTag("from", $p);
		    }, $p1->id);

Disp::is($b1."", "Bag(5):
  Variant(chr1[152555542, 152555559], chr1[152587723, 152587740])[from=>John]
  Variant(chr11[54951647, 54951658], chr11[54982904, 54982920])[from=>John]
  Variant(chr3[162512135, 162512152], chr3[162626319, 162626335])[from=>John]
  Variant(chr6[157548448, 157548897], chr6[157613161, 157613245])[from=>John]
  Variant(chr9[66862793, 66862814], chr9[69000092, 69000113])[from=>John]",
   "Bag 1 contents");

my $b2 = NGS::SV::Bag->new($p2);
$b2->forallVariants( sub { shift->doTag("from", $p2->id) } );
Disp::is($b2."", "Bag(8):
  Variant(chr11[54950612, 54950698], chr11[54984905, 54984917])[from=>Jack]
  Variant(chr11[54951612, 54951698], chr11[54982905, 54982917])[from=>Jack]
  Variant(chr2[152555542, 152555559], chr2[152587723, 152587740])[from=>Jack]
  Variant(chr3[162512146, 162512178], chr3[162626345, 162626357])[from=>Jack]
  Variant(chr6[157548448, 157548897], chr8[157613161, 157613245])[from=>Jack]
  Variant(chr9[66862868, 66862926], chr9[69000133, 69000110])[from=>Jack]
  Variant(chr9[66863000, 66863050], chr9[69000150, 69000170])[from=>Jack]
  Variant(chr9[66863060, 66863070], chr9[69002150, 69002170])[from=>Jack]",
   "Bag 2 contents");

my $res="";
$b1->intersectChr($b2, sub {
    my ($b1, $b2, $s1, $s2, $p1, $p2)=@_;
    $s1++; $s2++;
    $res .= "IntersectionChr b1[${s1}-${p1}] // b2[${s2}-${p2}]\n";
		  }
    );
Disp::is($res, "IntersectionChr b1[2-2] // b2[1-2]
IntersectionChr b1[3-3] // b2[4-4]
IntersectionChr b1[5-5] // b2[6-8]
", "intersectChr(b1, b2)");

$res="";
$b1->intersectPos1($b2, sub {
    my ($b1, $b2, $v1, $v2, $p1, $p2)=@_;
    $p1++; $p2++;
    $res .= "IntersectionPos1 b1:${p1}//b2:${p2} $v1 // $v2\n";
		  }
    );
Disp::is($res, "IntersectionPos1 b1:2//b2:2 Variant(chr11[54951647, 54951658], chr11[54982904, 54982920])[from=>John] // Variant(chr11[54951612, 54951698], chr11[54982905, 54982917])[from=>Jack]
IntersectionPos1 b1:3//b2:4 Variant(chr3[162512135, 162512152], chr3[162626319, 162626335])[from=>John] // Variant(chr3[162512146, 162512178], chr3[162626345, 162626357])[from=>Jack]
IntersectionPos1 b1:5//b2:6 Variant(chr9[66862793, 66862814], chr9[69000092, 69000113])[from=>John] // Variant(chr9[66862868, 66862926], chr9[69000133, 69000110])[from=>Jack]
IntersectionPos1 b1:5//b2:7 Variant(chr9[66862793, 66862814], chr9[69000092, 69000113])[from=>John] // Variant(chr9[66863000, 66863050], chr9[69000150, 69000170])[from=>Jack]
IntersectionPos1 b1:5//b2:8 Variant(chr9[66862793, 66862814], chr9[69000092, 69000113])[from=>John] // Variant(chr9[66863060, 66863070], chr9[69002150, 69002170])[from=>Jack]
", "intersectPos1(b1, b2)");

$res="";
$b1->intersect($b2, sub {
    my ($b1, $b2, $v1, $v2, $p1, $p2)=@_;
    $p1++; $p2++;
    $res .= "Intersection b1:${p1}//b2:${p2} $v1 // $v2\n";
		  }
    );
Disp::is($res, "Intersection b1:2//b2:2 Variant(chr11[54951647, 54951658], chr11[54982904, 54982920])[from=>John] // Variant(chr11[54951612, 54951698], chr11[54982905, 54982917])[from=>Jack]
Intersection b1:3//b2:4 Variant(chr3[162512135, 162512152], chr3[162626319, 162626335])[from=>John] // Variant(chr3[162512146, 162512178], chr3[162626345, 162626357])[from=>Jack]
Intersection b1:5//b2:6 Variant(chr9[66862793, 66862814], chr9[69000092, 69000113])[from=>John] // Variant(chr9[66862868, 66862926], chr9[69000133, 69000110])[from=>Jack]
Intersection b1:5//b2:7 Variant(chr9[66862793, 66862814], chr9[69000092, 69000113])[from=>John] // Variant(chr9[66863000, 66863050], chr9[69000150, 69000170])[from=>Jack]
", "intersect(b1, b2)");

use_ok('NGS::SV::BagsCollection');

my $bags=NGS::SV::BagsCollection->new('peoples' => [$p1, $p2, $p3, $p4]);
diag("initial bags\n".$bags);

$bags->countDuplicates;
diag("counted bags\n".$bags);

my $ubags=$bags->filter(sub {
    my $v=shift;
    return $v->tag("count")==1;
			      });
diag("filtered unique bags\n".$ubags);
