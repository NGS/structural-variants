#!/usr/bin/perl

use strict;
use warnings;
use Carp;

use NGS::SV;
use NGS::SV::People;
use NGS::SV::Input::LongRangerBEDPE;
use NGS::SV::Input::Breakdancer;
use NGS::SV::Input::LumpyBEDPE;
use NGS::SV::Input::Reference;

use NGS::SV::Input::General;
use List::Util qw(uniq);

sub get_people {
    my $id=shift;
    my $path=shift;
    my $ext=shift;
    my $format=shift;

    my $files=NGS::SV::Input::General::PatientList1($path, $ext);

    my %people;
    print "Loading $id\n";
    foreach my $file (@{$files}) {
	$file =~ /(B00[A-Z1-9]+)/;
	#DEBUG    print STDERR $1, "\n";
	$people{$1}=NGS::SV::People->new(
	    'id' => $1,
	    'filename' => $file,
	    'format' => $format,
	    'min-length' => 50000,
	    );
    }
    print "Loaded $id\n";
    return \%people;
}

my $people;
### GESTION DES DONNEES ILLUMINA LUMPY ###
#$people->{'IL'}=get_people('IL', "../10xChromium/Illumina_Lumpy", ".lumpy.bedpe", 'LumpyBEDPE');
### GESTION DES DONNEES LONGRANGER LUMPY ###
#$people->{'LRL'}=get_people('LRL', "../10xChromium/LongR_Lumpy", ".lumpy.bedpe", 'LumpyBEDPE');
### GESTION DE LA REFERENCE ###
$people->{'REF'}=get_people('REF', "../10xChromium/SV_valide_Kevin/ref", ".csv", 'Reference');
### GESTION DES DONNEES BREAKDANCER ###
$people->{'BD'}=get_people('BD', "../10xChromium/Illumina_Breakdancer", ".tab_annote", 'Breakdancer');
### GESTION DES DONNEES LONGRANGER ###
$people->{'LR'}=get_people('LR', "../10xChromium/LongR", "_candidates.bedpe", 'LongRangerBEDPE');
### GESTION DES DONNEES LONGRANGER ###
$people->{'LRCall'}=get_people('LRCall', "../10xChromium/LongR", "_calls.bedpe", 'LongRangerBEDPE');

    
#foreach my $elem (keys(%h_LR)) {
#    print $h_LR{$elem}, "\n";
#}

sub info_bags {
    my $bags=shift;
    my $res='';
    $bags->foreachBag( sub {
	my $b=shift;
	my $id=$b;
	while ($id->isa('NGS::SV::Bag')) {
	    $id=$id->id;
	}
	$res .= join('', $id->id,
	       "(", $b->nbVariants, " variants)",
	       "\n");
		       });
    return $res;
}

sub vstat {
    my $found=shift;
    my $tot=shift;
    #croak "DIV0" if $tot == 0;
    return $found." (".(int($found*100000/$tot)/1000).
	" %) variants found ($tot initially)";
}

sub info_filtered_bags {
    my $bags=shift;
    my $res=shift;
    my $str;
    $bags->foreachBag( sub {
	my $b=shift;
	my $id=$b;
	while ($id->isa('NGS::SV::Bag')) {
	    $id=$id->id;
	}
	my $nvfv=$b->nbVariants;
	my $nbv=$b->id->nbVariants;
	my $patient=$id->id;
	my $info=$b->id->tag('method');
	my $nbv_found=$nbv-$nvfv;
	
	$str.=join('', $patient, '/', $info, ": ",
		   vstat($nbv_found, $nbv), "\n");
	$res->{$info}->{found}+=$nbv_found;
	$res->{$info}->{total}+=$nbv;
		       });
    return $str;
}


use NGS::SV::BagsCollection;    

sub filter_bags {
    my $h=shift;
    
    my $bags=new NGS::SV::BagsCollection('peoples' => [ values(%$h) ]);
    print "Après chargement: \n", info_bags($bags);
    $bags->countDuplicates;
    
    if (0) {
	# Affiche les variants en doublon avant de les filtrer
	$bags->forallVariants(sub {
	    my ($b,$v)=@_;
	    if ($v->tag("count")>1) {
		print "$v\n";
	    }
				 });
    }
    
    my $bags_filtered=$bags->filter(sub {
	my $v=shift;
    return $v->tag("count")==1;
					  });
    print "Après filtrage: \n", info_bags($bags_filtered);

    return ($bags_filtered,$bags);
}

my $bags;
for my $m (sort keys %$people) {
    print "Filtering $m\n";
    ($bags->{'filtered'}->{$m}, $bags->{'init'}->{$m})=
	filter_bags($people->{$m});
    print "Filtered $m\n";
}

my @list_patients=sort {$a cmp $b} (List::Util::uniq (map { keys %{$people->{$_}} } (keys %$people)));
print "Patients: ".join(", ", @list_patients)."\n";

my $bfmp;
foreach my $m (keys %$people) {
    $bags->{'filtered'}->{$m}->foreachBag(
	sub {
	    my $b=shift;
	    $bfmp->{$m}->{$b->id->id->id}=$b;
	    $b->doTag('method', $m);
	    #print "storing $m/", $b->id->id->id, "\n";
	});
}
#foreach my $m ('BD') { # (keys %$people) {

sub cmp_m {
    my $m1=shift;
    my $m2=shift;
    if ($m1 eq 'REF') {
	if ($m2 eq 'REF') {
	    return 0;
	}
	return -1;
    } elsif ($m2 eq 'REF') {
	return 1;
    } else {
	return $m1 cmp $m2;
    }
}
my $gstats='';
my @m=sort { cmp_m($a, $b) } (keys %$people);
foreach my $m1 (@m) {
    foreach my $m2 (@m) {
	next if (cmp_m($m1, $m2) >= 0);
	#foreach my $p ('B00I2FB') { #(@list_patients) {
	my ($nbvm1f, $nbvm1t, $nbvm2f, $nbvm2t, $nbp)=(0,0,0,0);
	my $stats={};
	foreach my $p (@list_patients) {
	    if (not exists($bfmp->{$m1}->{$p})) {
		print STDERR "No patient $p for method $m1\n";
		next;
	    }
	    if (not exists($bfmp->{$m2}->{$p})) {
		print STDERR "No patient $p for reference $m1\n";
		next;
	    }
	    $nbp++;
	    
	    my $lbags=new NGS::SV::BagsCollection(
		'_bags' => [ 
		    $bfmp->{$m1}->{$p},
		    $bfmp->{$m2}->{$p},
		]);
	    #print "\nAvant analyse $m1/$p: \n", info_bags($lbags);
	    $lbags->foreachVariant(
		sub {
		    my ($b, $v)=@_;
		    $v->doTag("count", 1);
		    $v->doTag("origin", $b->tag("method"));
		});
	
	    $lbags->countDuplicates(
		sub {
		    my ($b1, $b2, $v1, $v2)=@_;
		    $v1->doTag("origin", $v1->tag("origin")."/".$b2->tag("method"));
		    $v2->doTag("origin", $v2->tag("origin")."/".$b1->tag("method"));	
		} 
		);
	    my $bags_filtered=$lbags->filter(
		sub {
		    my $v=shift;
		    return $v->tag("count")==1;
		});
	    
	    #print "Après analyse $m1/$p: \n", info_bags($bags_filtered);
	    print "\n", info_filtered_bags($bags_filtered, $stats);
	    
	    if (1) {
		# Affiche les variants en doublon avant de les filtrer
		$lbags->forallVariants(sub {
		    my ($b,$v)=@_;
		    if ($v->tag("count")>1) {
			print $b->tag('method').": $v\n";
		    }
				       });
	    }
	    #print $lbags;
	}
	{
	    my @lm = sort { cmp_m($a, $b) } (keys %$stats);
	    $gstats.=join('/', @lm).": ".$nbp." patients\n";
	    foreach my $m (@lm) {
		my $s=$stats->{$m};
		$gstats.= "$m: ".vstat($s->{found},$s->{total})."\n";
	    }
	}
    }
}
print "\n*********\n";
print "$gstats";
print "*********\n";
    

if(0) {
foreach my $p (@list_patients) {
    my $m='[LR|BD]';
    my $lbags=new NGS::SV::BagsCollection(
	'_bags' => [ 
	    $bfmp->{'LR'}->{$p},
	    $bfmp->{'BD'}->{$p},
	]);
    print "Avant analyse $m/$p: \n", info_bags($lbags);
    $lbags->foreachVariant(
	sub {
	    my (undef, $v)=@_;
	    $v->doTag("count", 1);
	});
    
    $lbags->countDuplicates;
    my $bags_filtered=$lbags->filter(
	sub {
	    my $v=shift;
	    return $v->tag("count")==1;
	});
    print "Après analyse $m/$p: \n", info_bags($bags_filtered);
}
}

print "\n";


#my $p1 = NGS::SV::People->new('id' => 'John');
#my $p2 = NGS::SV::People->new('id' => 'Jack');
#my $p3 = NGS::SV::People->new('id' => 'B00IOF8');
#my $p4 = NGS::SV::People->new('id' => 'testlumpy');

#my $tab_LR_var=NGS::SV::Input::LongRangerBEDPE::read("fic_longranger");
#my $tab_LR_var2=NGS::SV::Input::LongRangerBEDPE::read("fic_longranger2");
#my $tab_BD_var=NGS::SV::Input::Breakdancer::read("fic_breakdancer");
#my $tab_Lumpy_var=NGS::SV::Input::LumpyBEDPE::read("fic_lumpy_bedpe");

#$p1->addVariants(@{$tab_LR_var});
#$p2->addVariants(@{$tab_LR_var2});
#$p3->addVariants(@{$tab_BD_var});
#$p4->addVariants(@{$tab_Lumpy_var});

#print $p1, "\n";
#print $p2, "\n";
#print $p3, "\n";
#print $p4, "\n";

#for my $t (
#    [ $p1, 0, $p2, 0, 0],
#    [ $p1, 1, $p2, 1, -1],
#    [ $p1, 2, $p2, 2, -1],    
#    [ $p1, 3, $p2, 3, 0],    
#    [ $p1, 4, $p2, 4, 0],    
#    [ $p1, 4, $p2, 5, 1],    
#    ) {
#    my ($p1, $nv1, $p2, $nv2, $expected) = @{$t};
#    my $v1=($p1->variants)[$nv1];
#    my $v2=($p2->variants)[$nv2];
#    my $res=$v1 cmp $v2;
#    if ($res != $expected) {
#	print "ARGS: ".$p1->id."(".$v1.") cmp ".$p2->id."(".$v2.") == ".$res." (expected $expected)\n";
#    }
#}
